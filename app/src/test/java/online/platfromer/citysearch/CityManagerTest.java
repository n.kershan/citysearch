package online.platfromer.citysearch;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import online.platfromer.citysearch.models.CityModel;
import online.platfromer.citysearch.models.Coord;

public class CityManagerTest {

    private CityManager mCityManager;

    @Before
    public void setUp() {
        mCityManager = new CityManager();

        ArrayList<CityModel> initialList = new ArrayList<>();

        initialList.add(new CityModel(
                "US",
                "Alabama",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "US",
                "Albuquerque",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "US",
                "Anaheim",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "US",
                "Arizona",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "AU",
                "Sydney",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "EE",
                "Aa",
                0,
                new Coord(
                        0d,
                        0d)));

        mCityManager.setCitiesList(initialList);
    }

    @Test
    public void parseJsonToCitiesArrayTest() {
        String json = "[{\"country\":\"UA\",\"name\":\"Hurzuf\",\"_id\":707860,\"coord\":{\"lon\":34.283333,\"lat\":44.549999}}," +
                "{\"country\":\"RU\",\"name\":\"Novinki\",\"_id\":519188,\"coord\":{\"lon\":37.666668,\"lat\":55.683334}}]";

        ArrayList<CityModel> expected = new ArrayList<>();

        expected.add(new CityModel(
                "UA",
                "Hurzuf",
                707860,
                new Coord(
                        34.283333,
                        44.549999)));

        expected.add(new CityModel(
                "RU",
                "Novinki",
                519188,
                new Coord(
                        37.666668,
                        55.683334)));

        ArrayList<CityModel> result = mCityManager.parseJsonToCitiesArray(json);

        Assert.assertEquals(expected.size(), result.size());

        Assert.assertEquals(expected.get(0).getCountry(), result.get(0).getCountry());
        Assert.assertEquals(expected.get(0).getName(), result.get(0).getName());
        Assert.assertEquals(expected.get(0).getId(), result.get(0).getId());

        Assert.assertEquals(expected.get(0).getCoord().getLon(), result.get(0).getCoord().getLon());
        Assert.assertEquals(expected.get(0).getCoord().getLat(), result.get(0).getCoord().getLat());

        Assert.assertEquals(expected.get(1).getCountry(), result.get(1).getCountry());
        Assert.assertEquals(expected.get(1).getName(), result.get(1).getName());
        Assert.assertEquals(expected.get(1).getId(), result.get(1).getId());

        Assert.assertEquals(expected.get(1).getCoord().getLon(), result.get(1).getCoord().getLon());
        Assert.assertEquals(expected.get(1).getCoord().getLat(), result.get(1).getCoord().getLat());
    }

    @Test
    public void sortCitiesListTest() {
        CityManager cityManager = new CityManager();

        ArrayList<CityModel> initialList = new ArrayList<>();

        initialList.add(new CityModel(
                "Australia",
                "Sydney",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "US",
                "Denver",
                0,
                new Coord(
                        0d,
                        0d)));

        initialList.add(new CityModel(
                "US",
                "Anaheim",
                0,
                new Coord(
                        0d,
                        0d)));

        cityManager.setCitiesList(initialList);
        cityManager.sortCityList();

        ArrayList<CityModel> result = cityManager.getCitiesList();
        Assert.assertEquals(3, result.size());
        Assert.assertEquals("Anaheim", result.get(0).getName());
        Assert.assertEquals("Denver", result.get(1).getName());
        Assert.assertEquals("Sydney", result.get(2).getName());
    }

    @Test
    public void filterCityListSingleCharTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("A");
        Assert.assertEquals(5, result.size());
        Assert.assertEquals("Alabama", result.get(0).getName());
        Assert.assertEquals("Albuquerque", result.get(1).getName());
        Assert.assertEquals("Anaheim", result.get(2).getName());
        Assert.assertEquals("Arizona", result.get(3).getName());
        Assert.assertEquals("Aa", result.get(4).getName());
    }

    @Test
    public void filterCityListSingleCharLowerTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("s");
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Sydney", result.get(0).getName());
    }

    @Test
    public void filterCityListFirstTwoCharTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("Al");
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("Alabama", result.get(0).getName());
        Assert.assertEquals("Albuquerque", result.get(1).getName());
    }

    @Test
    public void filterCityListFirsThreeCharTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("Alb");
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Albuquerque", result.get(0).getName());
    }

    @Test
    public void filterCityListEmptyTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("");
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void filterCityListNullTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList(null);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void filterCityListBlankTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("Alb ");
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Albuquerque", result.get(0).getName());
    }

    @Test
    public void filterCityListShortTest() {
        ArrayList<CityModel> result = mCityManager.filterCityList("Aaa");
        Assert.assertEquals(0, result.size());
    }
}