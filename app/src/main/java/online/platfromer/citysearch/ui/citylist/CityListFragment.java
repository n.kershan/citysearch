package online.platfromer.citysearch.ui.citylist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import online.platfromer.citysearch.R;
import online.platfromer.citysearch.models.CityModel;
import online.platfromer.citysearch.models.Coord;

public class CityListFragment extends Fragment implements CityListAdapter.CityClickListener, TextWatcher {

    private OnFragmentInteractionListener mListener;
    private CityListAdapter mCityListAdapter;

    public CityListFragment() {
        // Required empty public constructor
    }

    public static CityListFragment newInstance() {
        return new CityListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_city_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.city_list_recycler_view);
        EditText searchEditText = view.findViewById(R.id.city_list_search_bar);

        searchEditText.addTextChangedListener(this);

        if (getActivity() != null) {
            mCityListAdapter = new CityListAdapter(this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                    getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(mCityListAdapter);

            mListener.fetchCities();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnClick(Coord coord) {
        mListener.showMapFragment(coord);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mListener.fetchFilteredCities(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void updateCitiesList(ArrayList<CityModel> cityModels) {
        mCityListAdapter.updateList(cityModels);
    }

    public interface OnFragmentInteractionListener {
        void showMapFragment(Coord coord);

        void fetchCities();

        void fetchFilteredCities(String filterBy);
    }
}
