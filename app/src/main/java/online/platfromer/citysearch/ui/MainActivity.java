package online.platfromer.citysearch.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import online.platfromer.citysearch.CityManager;
import online.platfromer.citysearch.R;
import online.platfromer.citysearch.models.CityModel;
import online.platfromer.citysearch.models.Coord;
import online.platfromer.citysearch.ui.citylist.CityListFragment;
import online.platfromer.citysearch.ui.citymap.CityMapFragment;

public class MainActivity extends AppCompatActivity implements
        CityListFragment.OnFragmentInteractionListener,
        CityMapFragment.OnFragmentInteractionListener {

    private static final String FRAGMENT_BACK_STACK_TAG = "fragment_back_stack_tag";

    private CityManager mCityManager = new CityManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CityListFragment.newInstance())
                .addToBackStack(FRAGMENT_BACK_STACK_TAG)
                .commit();

        new LoadCityList().execute();
    }

    @Override
    public void showMapFragment(Coord coord) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CityMapFragment.newInstance(coord.getLon(), coord.getLat()))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void fetchCities() {
        if (mCityManager.isLoaded()) {
            updateCityListFragmentData(mCityManager.getCitiesList());
        } else {
            new LoadCityList().execute();
        }
    }

    @Override
    public void fetchFilteredCities(String filterBy) {
        new FilterCityList().execute(filterBy);
    }

    @Override
    public void onFragmentInteraction() {

    }

    private class LoadCityList extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            mCityManager.loadCities(getAssets());
            mCityManager.sortCityList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            updateCityListFragmentData(mCityManager.getCitiesList());
        }
    }

    private class FilterCityList extends AsyncTask<String, Void, ArrayList<CityModel>> {

        @Override
        protected ArrayList<CityModel> doInBackground(String... args) {
            String filterBy = args[0];
            return mCityManager.filterCityList(filterBy);
        }

        @Override
        protected void onPostExecute(ArrayList<CityModel> cityModels) {
            super.onPostExecute(cityModels);

            updateCityListFragmentData(cityModels);
        }
    }

    private void updateCityListFragmentData(ArrayList<CityModel> cityModels) {
        CityListFragment cityListFragment = (CityListFragment) getSupportFragmentManager().findFragmentById(R.id.container);

        if (cityListFragment != null) {
            cityListFragment.updateCitiesList(cityModels);
        }
    }
}
