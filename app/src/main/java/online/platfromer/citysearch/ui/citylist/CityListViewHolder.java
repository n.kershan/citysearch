package online.platfromer.citysearch.ui.citylist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import online.platfromer.citysearch.R;

class CityListViewHolder extends RecyclerView.ViewHolder {

    private TextView mCityName;

    CityListViewHolder(@NonNull View itemView) {
        super(itemView);

        mCityName = itemView.findViewById(R.id.city_item_name_label);
    }

    void setCityName(String name) {
        mCityName.setText(name);
    }
}
