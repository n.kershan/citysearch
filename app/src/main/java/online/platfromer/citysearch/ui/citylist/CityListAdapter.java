package online.platfromer.citysearch.ui.citylist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import online.platfromer.citysearch.R;
import online.platfromer.citysearch.models.CityModel;
import online.platfromer.citysearch.models.Coord;

public class CityListAdapter extends RecyclerView.Adapter<CityListViewHolder> {

    private ArrayList<CityDisplayModel> cityDisplayModels;
    private CityClickListener mCityClickListener;

    CityListAdapter(CityClickListener cityClickListener) {
        mCityClickListener = cityClickListener;
    }

    @NonNull
    @Override
    public CityListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CityListViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.view_city_item,
                viewGroup,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CityListViewHolder cityListViewHolder, int position) {
        cityListViewHolder.setCityName(cityDisplayModels.get(position).getName());
        cityListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CityDisplayModel cityDisplayModel =
                        cityDisplayModels.get(cityListViewHolder.getAdapterPosition());
                mCityClickListener.OnClick(cityDisplayModel.getCoord());
            }
        });
    }

    @Override
    public int getItemCount() {
        return cityDisplayModels != null ? cityDisplayModels.size() : 0;
    }

    void updateList(ArrayList<CityModel> citiesList) {
        cityDisplayModels = new ArrayList<>();

        for (CityModel cityModel :
                citiesList) {
            String name = cityModel.getName() + ", " + cityModel.getCountry();
            CityDisplayModel cityDisplayModel = new CityDisplayModel();
            cityDisplayModel.setName(name);
            cityDisplayModel.setCoord(cityModel.getCoord());
            cityDisplayModels.add(cityDisplayModel);
        }

        notifyDataSetChanged();
    }

    public interface CityClickListener {
        void OnClick(Coord coord);
    }
}
