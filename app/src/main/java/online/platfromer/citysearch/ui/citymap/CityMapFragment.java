package online.platfromer.citysearch.ui.citymap;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import online.platfromer.citysearch.R;

public class CityMapFragment extends Fragment implements OnMapReadyCallback {
    public static final String INTENT_CITY_LON = "intent_city_lon";
    public static final String INTENT_CITY_LAT = "intent_city_lat";

    MapView mMapView;
    private GoogleMap mMap;

    private double mCityLat;
    private double mCityLon;

    private OnFragmentInteractionListener mListener;

    public CityMapFragment() {
        // Required empty public constructor
    }

    public static CityMapFragment newInstance(Double lon, Double lat) {
        CityMapFragment fragment = new CityMapFragment();
        Bundle args = new Bundle();
        args.putDouble(INTENT_CITY_LON, lon);
        args.putDouble(INTENT_CITY_LAT, lat);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCityLon = getArguments().getDouble(INTENT_CITY_LON);
            mCityLat = getArguments().getDouble(INTENT_CITY_LAT);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_city_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng cityLatLng = new LatLng(mCityLat, mCityLon);
        mMap.addMarker(new MarkerOptions().position(cityLatLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityLatLng));
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}
