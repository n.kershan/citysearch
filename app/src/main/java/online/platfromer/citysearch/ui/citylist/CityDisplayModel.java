package online.platfromer.citysearch.ui.citylist;

import online.platfromer.citysearch.models.Coord;

class CityDisplayModel {
    private String name;
    private Coord coord;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    Coord getCoord() {
        return coord;
    }

    void setCoord(Coord coord) {
        this.coord = coord;
    }
}
