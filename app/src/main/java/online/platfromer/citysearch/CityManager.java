package online.platfromer.citysearch;

import android.content.res.AssetManager;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import online.platfromer.citysearch.models.CityModel;

public class CityManager {

    private ArrayList<CityModel> mCitiesList;

    public ArrayList<CityModel> getCitiesList() {
        return mCitiesList;
    }

    void setCitiesList(ArrayList<CityModel> mCitiesList) {
        this.mCitiesList = mCitiesList;
    }

    public void loadCities(AssetManager assetInput) {
        String json = fetchFile(assetInput);
        mCitiesList = parseJsonToCitiesArray(json);
    }

    @Nullable
    private String fetchFile(AssetManager assetInput) {
        String jsonString = null;
        try {
            InputStream inputStream = assetInput.open("cities.json");
            byte[] buffer = new byte[inputStream.available()];
            int numberOfBytes = inputStream.read(buffer);

            if (numberOfBytes > 0) {
                jsonString = new String(buffer, "UTF-8");
            }
            inputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    ArrayList<CityModel> parseJsonToCitiesArray(String json) {
        return new Gson().fromJson(json, new TypeToken<ArrayList<CityModel>>() {
        }.getType());
    }

    public void sortCityList() {
        mCitiesList = sortCityList(mCitiesList);
    }

    private ArrayList<CityModel> sortCityList(ArrayList<CityModel> citiesList) {
        Collections.sort(citiesList, new Comparator<CityModel>() {
            @Override
            public int compare(CityModel o1, CityModel o2) {
                int nameComparator = o1.getName().compareTo(o2.getName());

                if (nameComparator != 0) {
                    return nameComparator;
                }

                return o1.getCountry().compareTo(o2.getCountry());
            }
        });

        return citiesList;
    }

    public ArrayList<CityModel> filterCityList(String filterBy) {
        ArrayList<CityModel> filteredList = new ArrayList<>();

        if (filterBy == null || filterBy.isEmpty()) {
            return mCitiesList;
        }

        int filterStringLength = filterBy.length();
        String formattedFilterBy = filterBy.toLowerCase().trim();

        for (CityModel cityModel :
                mCitiesList) {
            if (cityModel.getName().length() < filterStringLength) {
                continue;
            }

            String name =
                    cityModel.getName().substring(0, filterStringLength).toLowerCase().trim();

            if (name.contains(formattedFilterBy)) {
                filteredList.add(cityModel);
            }
        }

        return filteredList;
    }

    public boolean isLoaded() {
        return mCitiesList != null && !mCitiesList.isEmpty();
    }
}
